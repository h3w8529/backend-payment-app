import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { Logger } from '@nestjs/common';
import * as config from 'config';
import { AppModule } from './app.module';

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const paymentAppConfig = config.get('payment-app-microservice');

  const port = paymentAppConfig.port;
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.TCP,
    options: {
      host: paymentAppConfig.host,
      port: port,
    },
  });

  app.listen(() => {
    logger.log(`Payment App Microservice is listening at port ${port}...`);
  });
}
bootstrap();
