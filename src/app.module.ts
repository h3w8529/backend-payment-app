import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OrdersModule } from './orders/orders.module';
import { typeOrmConfig } from './config/typeorm.config';
import { PaymentsModule } from './payments/payments.module';

@Module({
  imports: [PaymentsModule, TypeOrmModule.forRoot(typeOrmConfig), OrdersModule],
})
export class AppModule {}
