import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

import { OrderStatus } from './order-status.enum';

@Entity()
export class Order extends BaseEntity {
	@PrimaryGeneratedColumn('uuid') id: string;

	@Column({ nullable: true })
	orderNo: string;

	@Column() remark: string;

	@Column({ default: OrderStatus.CREATED })
	status: OrderStatus;

	@Column({ type: 'numeric' })
	amount: number;

	@Column({ nullable: true })
	paymentAt: Date;

	@Column({ nullable: true })
	confirmedAt: Date;

	@Column({ nullable: true })
	cancelledAt: Date;

	@Column({ nullable: true })
	deliveredAt: Date;

	@Column({ nullable: true, default: () => 'CURRENT_TIMESTAMP' })
	createdAt: Date;

	@Column({ nullable: true, default: () => 'CURRENT_TIMESTAMP' })
	updatedAt: Date;
}
