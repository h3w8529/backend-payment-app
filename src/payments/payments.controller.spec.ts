import { Test } from '@nestjs/testing';
import { PaymentsController } from './payments.controller';
import { PaymentsService } from './payments.service';
import { OrderRepository } from '../orders/order.repository';
import { PaymentRequestDto } from './dto/payment-request-dto';

const createdOrder = {
  id: 'ff1fc1dd-ad23-4d5d-9aed-b31867669463',
  orderNo: '#000008',
  remark: '12311',
  status: 'CREATED',
  amount: '2.31',
  paymentAt: null,
  confirmedAt: null,
  cancelledAt: null,
  deliveredAt: null,
  createdAt: '2020-06-11T20:48:53.564Z',
  updatedAt: '2020-06-11T20:48:53.564Z',
  save: () => jest.fn(),
};

const successResponse = { c: 1, m: 'successful' };
const unsuccessfulResponse = { c: 0, m: 'unsuccessful' };

const mockOrderRepository = () => ({
  findOne: jest.fn(),
});

describe('Payment Controller', () => {
  let paymentsController;
  let paymentsService;
  let orderRepository;

  describe('Test created order', () => {
    beforeEach(async () => {
      const module = await Test.createTestingModule({
        providers: [
          PaymentsService,
          { provide: OrderRepository, useFactory: mockOrderRepository },
          PaymentsController,
        ],
      }).compile();

      paymentsService = await module.get<PaymentsService>(PaymentsService);
      orderRepository = await module.get<OrderRepository>(OrderRepository);
      paymentsController = await module.get<PaymentsController>(
        PaymentsController,
      );
    });

    it('With valid signature and successful result', async () => {
      const order = createdOrder;
      orderRepository.findOne.mockResolvedValue(order);
      const paymentRequestParam = new PaymentRequestDto();
      paymentRequestParam.id = order.id;
      paymentRequestParam.signature =
        'ff1fc1ddad234d5d9aedb31867669463CREATED231';

      jest
        .spyOn(paymentsController, 'paymentRequest')
        .mockImplementation(async () => successResponse);

      const response = await paymentsController.paymentRequest(
        paymentRequestParam,
      );

      expect(response).toEqual(successResponse);
    });

    it('With valid signature and unsuccessful result', async () => {
      const order = createdOrder;
      orderRepository.findOne.mockResolvedValue(order);
      const paymentRequestParam = new PaymentRequestDto();
      paymentRequestParam.id = order.id;
      paymentRequestParam.signature =
        'ff1fc1ddad234d5d9aedb31867669463CREATED231';

      jest
        .spyOn(paymentsController, 'paymentRequest')
        .mockImplementation(async () => unsuccessfulResponse);

      const response = await paymentsController.paymentRequest(
        paymentRequestParam,
      );

      expect(response).toEqual(unsuccessfulResponse);
    });
  });
});
