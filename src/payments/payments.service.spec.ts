import { Test } from '@nestjs/testing';
import { PaymentsService } from './payments.service';
import { OrderRepository } from '../orders/order.repository';
import { NotFoundException } from '@nestjs/common';

const mockOrder = {
  id: 'ff1fc1dd-ad23-4d5d-9aed-b31867669463',
  orderNo: '#000008',
  remark: '12311',
  status: 'DELIVERED',
  amount: '2.31',
  paymentAt: '2020-06-12T04:48:54.133Z',
  confirmedAt: '2020-06-12T04:48:54.603Z',
  cancelledAt: null,
  deliveredAt: '2020-06-12T04:49:05.003Z',
  createdAt: '2020-06-11T20:48:53.564Z',
  updatedAt: '2020-06-11T20:48:53.564Z',
};

const mockOrderRepository = () => ({
  findOne: jest.fn(),
});

describe('Payment Service', () => {
  let paymentsService;
  let orderRepository;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        PaymentsService,
        { provide: OrderRepository, useFactory: mockOrderRepository },
      ],
    }).compile();

    paymentsService = await module.get<PaymentsService>(PaymentsService);
    orderRepository = await module.get<OrderRepository>(OrderRepository);
  });

  describe('Get Order', () => {
    it('Get order from the payment services', async () => {
      orderRepository.findOne.mockResolvedValue(mockOrder);
      const result = await paymentsService.getOrderById(mockOrder.id);
      expect(orderRepository.findOne).toHaveBeenCalled();
      expect(result).toEqual(mockOrder);
    });

    it('Throw an error as order is not found', () => {
      orderRepository.findOne.mockResolvedValue(null);
      expect(paymentsService.getOrderById(mockOrder.id)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
});
