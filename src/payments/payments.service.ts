import { Injectable, NotFoundException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { OrderRepository } from '../orders/order.repository';
import { Order } from '../orders/order.entity';

@Injectable()
export class PaymentsService {
  constructor(
    @InjectRepository(OrderRepository)
    private orderRepository: OrderRepository,
  ) {}

  async getOrderById(id: string): Promise<Order> {
    const found = await this.orderRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Order with the ID ${id} is not found.`);
    }
    return found;
  }
}
