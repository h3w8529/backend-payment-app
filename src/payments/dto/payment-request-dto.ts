import { IsNotEmpty } from 'class-validator';

export class PaymentRequestDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  signature: string;
}
