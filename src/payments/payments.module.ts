import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentsService } from './payments.service';
import { PaymentsController } from './payments.controller';
import { OrderRepository } from '../orders/order.repository';

@Module({
  imports: [TypeOrmModule.forFeature([OrderRepository])],
  providers: [PaymentsService],
  controllers: [PaymentsController],
})
export class PaymentsModule {}
