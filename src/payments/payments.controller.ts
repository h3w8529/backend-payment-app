import { Controller, Logger, BadRequestException } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { of } from 'rxjs';

import { PaymentsService } from './payments.service';
import { PaymentRequestDto } from './dto/payment-request-dto';
import { OrderStatus } from '../orders/order-status.enum';

@Controller()
export class PaymentsController {
  private logger = new Logger('PaymentController');
  private AVAILABLE_PAYMENT_RESPONSE = [
    { c: 0, m: 'unsuccessful' },
    { c: 1, m: 'successful' },
  ];

  constructor(private paymentsService: PaymentsService) {}

  private getRandomPaymentResponse = (): number => {
    return Math.floor(
      Math.random() * (this.AVAILABLE_PAYMENT_RESPONSE.length - 1 - 0 + 1) + 0,
    );
  };

  @MessagePattern('paymentRequest')
  async paymentRequest(payload: PaymentRequestDto): Promise<any> {
    try {
      const { id, signature } = payload;
      const order = await this.paymentsService.getOrderById(id);

      if (!!order.paymentAt) {
        throw new BadRequestException(`This order is already paid.`);
      }
      if (![OrderStatus.CREATED].includes(order.status)) {
        throw new BadRequestException(
          `This order's is already ${order.status}`,
        );
      }

      // hash key signature
      const paymentSignature = `${id}${order.status}${order.amount}`
        .split('-')
        .join('')
        .split('.')
        .join('');

      // validate signature
      if (paymentSignature !== signature) {
        throw new BadRequestException(`Invalid signature`);
      }

      const paymentStatus = this.AVAILABLE_PAYMENT_RESPONSE[
        this.getRandomPaymentResponse()
      ];

      if (paymentStatus.c === 1) {
        order.paymentAt = new Date();
        await order.save();
      }
      return of(paymentStatus).pipe();
    } catch (e) {
      this.logger.error(e);
      return of(e).pipe();
    }
  }
}
